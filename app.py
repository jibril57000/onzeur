from flask_sqlalchemy import SQLAlchemy
from flask import Flask, render_template, request, session, redirect, url_for
import deezer
import hashlib
from youtubesearchpython import VideosSearch
from pytube import YouTube
import json
import os.path

app = Flask(__name__)
app.secret_key = "123"
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite3'
db = SQLAlchemy(app)

client = deezer.client.Client()


def getAlbum():
    return [artist.get_albums()[0] for artist in client.get_artists_chart()]


def getArtist():
    return client.get_artists_chart()


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(120), unique=True, nullable=False)


class Like(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    accountMail = db.Column(db.String(80), unique=False, nullable=False)
    songID = db.Column(db.String(80), unique=False, nullable=False)


@app.template_filter()
def convert(seconds):
    min = seconds // 60
    sec = seconds % 60
    sec = str(sec).zfill(2)
    return f"{str(min)}:{sec}"


@app.template_filter()
def number(i):
    n = str(i)
    return " ".join([n[::-1][i:i + 3] for i in range(0, len(n), 3)])[::-1]


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/account')
def account():
    if 'auth' in session.keys():
        return redirect(url_for("player"))
    else:
        return render_template("account.html")


@app.route('/player')
def player():
    if 'auth' in session.keys():
        return render_template("player.html", name=session["name"], email=session["email"], albums=getAlbum(),
                               artist=getArtist())

    else:
        return render_template("account.html")


@app.route('/albums/<name>')
def album(name):
    if 'auth' in session.keys():
        album = client.search_albums(name)[0]
        retrieveLike = Like.query.filter_by(accountMail=session["email"]).all()
        songId = [int(likes.songID) for likes in retrieveLike]
        print(songId)
        return render_template("albums.html", likes=songId, name=session["name"], email=session["email"], album=album)

    else:
        return render_template("account.html")


@app.route('/artist/<name>')
def artist(name):
    if 'auth' in session.keys():
        artist = client.search_artists(name)[0]
        return render_template("artist.html", name=session["name"], email=session["email"], artist=artist)

    else:
        return render_template("account.html")


@app.route('/dis')
def dis():
    session.clear()
    return redirect(url_for("index"))


@app.route("/signup", methods=["POST"])
def signup():
    name, email, password = request.form["id"], request.form["email"], request.form["pass"]
    print(name, email, password)  #
    user = User(username=name, email=email, password=hashlib.sha256(password.encode()).hexdigest())
    db.session.add(user)
    db.session.commit()
    session["auth"] = "1"
    session["name"] = name
    session["email"] = email
    return redirect(url_for('player'))


@app.route("/login", methods=["POST"])
def login():
    email, password = request.form["id"], request.form["pass"]
    user = User.query.filter_by(email=email).first()
    if user.password == hashlib.sha256(password.encode()).hexdigest():
        session["auth"] = "1"
        session["name"] = user.username
        session["email"] = email
        return redirect(url_for('player'))
    else:
        return redirect(url_for('account'))


@app.route("/api/like/<accountMail>/<songId>")
def l(accountMail, songId):
    # check if already like
    retrieveLike = Like.query.filter_by(accountMail=session["email"]).all()

    for like in retrieveLike:
        if like.songID == songId:
            # got to unlike
            Like.query.filter_by(songID=songId).delete()
            db.session.commit()
            return "200"

    print(accountMail, songId)
    l = Like(accountMail=str(accountMail), songID=str(songId))
    db.session.add(l)
    db.session.commit()
    return "200"


@app.route("/api/download/<artist>/<song>")
def download(artist, song):
    videosSearch = VideosSearch(f'{artist} {song}', limit=1)
    id = videosSearch.result()['result'][0]["id"]
    yt = YouTube(f'http://youtube.com/watch?v={id}')
    print(f'http://youtube.com/watch?v={id}')
    hash = hashlib.sha256(yt.title.encode()).hexdigest()

    if os.path.exists(f"static/songs/{hash}.mp4"):
        object = {"title": yt.title, "fileName": f"{hash}.mp4"}
        js = json.dumps(object)
        return js

    yt.streams.filter(progressive=True).first().download("static/songs", f"{hash}.mp4")
    object = {"title": yt.title, "fileName": f"{hash}.mp4"}
    js = json.dumps(object)

    return js


if __name__ == '__main__':
    app.run()
